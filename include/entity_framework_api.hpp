/*
The MIT License(MIT)

Copyright(c) 2014 Angelini, Bracci, Dominici

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files(the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions :

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#pragma once

// SSA
#include "entity_factory.hpp"
#include "component_factory.hpp"
#include "system_looper.hpp"


class EntityFrameworkAPI
{
public:
	EntityFrameworkAPI();
	~EntityFrameworkAPI();

	// ===== ENTITY-RELATED METHODS =====
	EntityHandle create_entity();

	EntityHandle get_entity_from_id(Entity::id_t p_id);

	EntityFactory& get_entity_factory() { return m_entity_factory; }

	// ===== COMPONENT-RELATED METHODS =====
	template <typename component_t, typename ...ctor_args>
	Component::id_t register_component(EntityHandle& p_entity_handle, ctor_args ...p_args);

	template <typename component_t>
	component_t& get_component(Component::id_t p_id);

	// TODO : unregister and unlink all entities linked to that component
	template <typename component_t>
	void unregister_component(Component::id_t p_id);

	ComponentFactory& get_component_factory() { return m_component_factory; }

	// ===== SYSTEM-RELATED METHODS ====
	void add_system(System& p_system);
	void remove_system(System& p_system);
	void process();

	template <typename component_t>
	void link_system(System& p_system);

	template <typename component_t>
	void unlink_system(System& p_system);

	SystemLooper& get_system_looper() { return m_system_looper; }

private:
	EntityFactory		m_entity_factory;
	ComponentFactory	m_component_factory;
	SystemLooper		m_system_looper;
};

template <typename component_t, typename ...ctor_args>
Component::id_t EntityFrameworkAPI::register_component(EntityHandle& p_entity_handle, ctor_args ...p_args)
{
	return m_component_factory.register_component<component_t>(p_entity_handle, p_args...);
}

template <typename component_t>
component_t& EntityFrameworkAPI::get_component(Component::id_t p_id)
{
	return m_component_factory.get_component<component_t>(m_component_factory.get_type_from_component<component_t>(), p_id);
}

template <typename component_t>
void EntityFrameworkAPI::unregister_component(Component::id_t p_id)
{
	return;
}

template <typename component_t>
void EntityFrameworkAPI::link_system(System& p_system)
{
	p_system.register_component(m_component_factory.get_type_from_component<component_t>());
}

template <typename component_t>
void EntityFrameworkAPI::unlink_system(System& p_system)
{
	p_system.unregister_component(m_component_factory.get_type_from_component<component_t>());
}
