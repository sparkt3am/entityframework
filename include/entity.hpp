/*
The MIT License(MIT)

Copyright(c) 2014 Angelini, Bracci, Dominici

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files(the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions :

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#pragma once

// SSA
#include "component.hpp"

// C++ STD
#include <cstdint>
#include <array>


class Entity
{
public:
	typedef std::uint64_t id_t;

	Entity();
	~Entity();

	template <typename component_t>
	component_t& get_component(Component::type_t p_type);

	void add_component(Component* c, Component::type_t type);

	bool has_component(Component::type_t type);

	std::uint64_t ref_count;
	id_t id;

private:
	Component* m_components[Component::max_component_number];
};

template <typename component_t>
component_t& Entity::get_component(Component::type_t p_type)
{
	return *(static_cast<component_t*>(m_components[static_cast<std::size_t>(p_type)]));
}

