/*
The MIT License(MIT)

Copyright(c) 2014 Angelini, Bracci, Dominici

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files(the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions :

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#pragma once

// C++ STD
#include <cstdint>
#include <bitset>


// Forward declaration
class Entity;

class Component
{
	friend class ComponentFactory;
public:
	const static std::uint64_t max_component_number{ 42 };

	typedef std::uint64_t id_t;
	typedef std::uint64_t type_t;

public:
	Component();

	// TODO ? dobbiamo togliere il link quando va out of scope ?
	~Component();

	type_t get_type()const { return m_type; }
	Entity* get_entity()const { return m_entity; }
	id_t get_id()const { return m_id; }

private:
	// Type of the component, can range from 0 to MAX_COMPONENT_NUMBER
	type_t  m_type;

	// Entity the Component is linked to 
	Entity* m_entity;

	id_t	m_id;
};