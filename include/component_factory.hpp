/*
The MIT License(MIT)

Copyright(c) 2014 Angelini, Bracci, Dominici

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files(the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions :

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#pragma once

// C++ STD
#include <cstdlib>
#include <array>
#include <unordered_map>
#include <typeinfo>

// SSA
#include "component.hpp"
#include "bag.hpp"
#include "entity_handle.hpp"
#include "entity.hpp"


class ComponentFactory
{
	typedef std::size_t type_hash_t;
public:
	ComponentFactory();
	~ComponentFactory();

	template <typename component_t, typename ...ctor_args>
	Component::id_t register_component(EntityHandle& e, ctor_args ...p_args);

	void remove_component(Component::type_t type, Component::id_t id);

	template <typename component_t>
	component_t& get_component(Component::type_t p_type, Component::id_t id);

	Bag& get_components_all(Component::type_t p_type) { return *m_components[static_cast<std::size_t>(p_type)]; }

	// Returns the type ( index ) from the actual type, this returns max_component + 1 if does not exists
	template <typename component_t>
	Component::type_t get_type_from_component()const;

private:
	std::array<Bag*, Component::max_component_number>	m_components;
	std::unordered_map<type_hash_t, Component::type_t>	m_types;
	std::size_t											m_last_type;
};

template <typename component_t, typename ...ctor_args>
Component::id_t ComponentFactory::register_component(EntityHandle& e, ctor_args ...p_args)
{
	type_hash_t hash = typeid(component_t).hash_code();
	auto find_res = m_types.find(hash);
	Component::type_t new_type;
		
	if (find_res == m_types.end()) // Type has not been registered yet
	{
		new_type = m_last_type++;
		m_types[hash] = new_type; // Adding it to type register
		m_components[static_cast<std::size_t>(new_type)] = new Bag(sizeof(component_t), 10); // Creating new bag
	}
	else
		new_type = find_res->second;

	Component::id_t id = m_components[static_cast<std::size_t>(new_type)]->add_object<component_t>(p_args...);
	Component& new_component = m_components[static_cast<std::size_t>(new_type)]->get_object<Component>(id);

	e.get().add_component(&new_component, new_type);

	// Filling out component's informations
	new_component.m_type = new_type;
	new_component.m_id = id;
	new_component.m_entity = &e.get();
		
	return id;
}

template <typename component_t>
component_t& ComponentFactory::get_component(Component::type_t p_type, Component::id_t id)
{
	return m_components[p_type]->get_object<component_t>(id);
}

template <typename component_t>
Component::type_t ComponentFactory::get_type_from_component()const
{
	auto find_res = m_types.find(typeid(component_t).hash_code());
	if (find_res == m_types.end())
		return Component::max_component_number + 1;
	return find_res->second;
}
