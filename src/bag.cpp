/*
The MIT License(MIT)

Copyright(c) 2014 Angelini, Bracci, Dominici

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files(the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions :

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

// Header
#include "bag.hpp"

// C++ STD
#include <algorithm>


Bag::Bag(std::size_t p_element_size, std::size_t p_initial_buffer_size) :
	m_data{ nullptr },
	m_element_size{ p_element_size },
	m_current_size{ 0 }
{
	std::size_t to_allocate = m_element_size * p_initial_buffer_size;

	m_data = new uint8_t[to_allocate];

	m_current_size = p_initial_buffer_size;

	// Generating free spots
	m_free_list.resize(static_cast<std::size_t>(m_current_size));
	index_t index{ 0 };
	std::generate(m_free_list.begin(), m_free_list.end(), [&] { return index++; });
	std::memset(m_data, 0, m_element_size * static_cast<std::size_t>(m_current_size));
}

Bag::~Bag()
{
	_safe_release();
}

void Bag::recycle(index_t p_index)
{
	// Erasing object
	std::memset(m_data + p_index * m_element_size, 0, m_element_size);

	// Adding to free list
	m_free_list.push_front(p_index);
}

void Bag::_safe_release()
{
	if (m_data != nullptr)
		delete[] m_data;
}

Bag::index_t Bag::_get_next_spot()
{
	index_t next_spot{ m_free_list.front() };
	m_free_list.pop_front();
	return next_spot;
}
