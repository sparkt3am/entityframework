/*
The MIT License(MIT)

Copyright(c) 2014 Angelini, Bracci, Dominici

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files(the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions :

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

// Header
#include "entity_factory.hpp"


EntityFactory::EntityFactory() :
	m_entities(sizeof(Entity), 50)
{
}

EntityHandle EntityFactory::create_entity()
{
	Entity::id_t id = m_entities.add_object<Entity>();
	Entity& e = m_entities.get_object<Entity>(id);
	e.id = id;
	return get_handle(id);
}

EntityHandle EntityFactory::get_handle(Entity::id_t id)
{
	return EntityHandle(m_entities.get_object<Entity>(id));
}

void EntityFactory::remove_handle(EntityHandle& handle)
{
	Entity& e = handle.get();
	e.ref_count--;
	if (e.ref_count <= 0)
		remove_entity(e.id);
	delete &handle;
}

void EntityFactory::remove_entity(Bag::index_t index)
{
	// Check if entity has components
	m_entities.recycle(index);
}
