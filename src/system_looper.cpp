/*
The MIT License(MIT)

Copyright(c) 2014 Angelini, Bracci, Dominici

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files(the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions :

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

// Header
#include "system_looper.hpp"

// SSA
#include "entity_factory.hpp"
#include "component_factory.hpp"

//C++ STD
#include <algorithm>


SystemLooper::SystemLooper(EntityFactory& p_entity_factory,
	ComponentFactory& p_component_factory) : 
	m_entity_factory{ &p_entity_factory },
	m_component_factory{ &p_component_factory }
{

}

SystemLooper::~SystemLooper()
{

}

void SystemLooper::add_system(System& p_system)
{
	m_systems.push_back(&p_system);
}

void SystemLooper::remove_system(System& p_system)
{
	// Exception if not found
	m_systems.erase(std::find(m_systems.begin(), m_systems.end(), &p_system));
}

void SystemLooper::process()
{
	for (auto& system : m_systems)
	{
		const auto& registered = system->get_registered_all();
		
		// Finding first registered component
		Component::type_t first_type = Component::max_component_number + 1;
		for (unsigned int i = 0; i < Component::max_component_number; ++i)
		{
			if (system->has_component_registered(i))
			{
				first_type = i;
				break;
			}
		}

		if (first_type == Component::max_component_number + 1)
			continue; // No components registered

		// Getting bag of components
		auto& first_bag = m_component_factory->get_components_all(first_type);

		// ""Iterating"" 
		uint8_t* data = first_bag.get_data_ptr();
		for (unsigned int i = 0; i < first_bag.get_last_element_pos(); ++i)
		{
			Component* component{ reinterpret_cast<Component*>(data + i * first_bag.get_element_size()) };
			if (component->get_entity() == nullptr) // Current spot is empty
				continue;

			Entity* next_entity{ component->get_entity() };

			bool valid{ true };
			for (unsigned int c = 0; c < Component::max_component_number; ++c)
			{
				if (registered[c] && !next_entity->has_component(c))
					valid = false;
			}

			if (valid)
				system->process(EntityHandle(*next_entity));
		}
	}
}
