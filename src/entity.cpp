/*
The MIT License(MIT)

Copyright(c) 2014 Angelini, Bracci, Dominici

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files(the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions :

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

// Header
#include "entity.hpp"

//C++ STD
#include <cstring>

Entity::Entity() :
	ref_count{ 0 },
	id{ 0 }
{
	// ODIO Visual Studio, default per i puntatori non e' 0x0000, ma 0x0c0c0c0 o qualche porcata simile , neanche gargabe
	std::memset(m_components, 0, sizeof(Component*)* Component::max_component_number);
}

Entity::~Entity()
{
}

void Entity::add_component(Component* c, Component::type_t type)
{
	//TODO when manager manager is ready
	m_components[type] = c;
}

bool Entity::has_component(Component::type_t p_type)
{
	if (m_components[static_cast<std::size_t>(p_type)] == nullptr)
		return false;
	return true;
}
