#include <iostream>

#include "component.hpp"
#include "system.hpp"
#include "entity_handle.hpp"
#include "entity_framework_api.hpp"

class CustomComponent : public Component
{
public:
	CustomComponent(float p_v)  { x = y = z = p_v; }
	float x, y, z;
};

class CustomComponent2 : public Component
{
public:
	CustomComponent2(const std::string& p_s) : m_string{ p_s } { }
	std::string m_string;
};

class CustomSystem : public System
{
public :
	void process(EntityHandle p_next_entity)override
	{
		std::cout << "C1" << std::endl;
		std::cout << p_next_entity.get().get_component<CustomComponent>(0).x << std::endl;
		std::cout << p_next_entity.get().get_component<CustomComponent2>(1).m_string << std::endl;
	}
};

class CustomSystem2 : public System
{
public:
	void process(EntityHandle p_next_entity)override
	{
		std::cout << "C2" << std::endl;
		std::cout << p_next_entity.get().get_component<CustomComponent>(0).x << std::endl;
		std::cout << p_next_entity.get().get_component<CustomComponent2>(1).m_string << std::endl;
	}
};

int main()
{
	EntityFrameworkAPI api;
	
	EntityHandle handle = api.create_entity();
	EntityHandle handle2 = api.create_entity();
	api.register_component<CustomComponent>(handle, 4.f);
	api.register_component<CustomComponent2>(handle, "ciao");

	CustomSystem system;

	api.link_system<CustomComponent>(system);

	CustomSystem2 system2;

	api.link_system<CustomComponent2>(system2);

	api.add_system(system);
	api.add_system(system2);

	while (true)
	{
		api.process();
	}
}

