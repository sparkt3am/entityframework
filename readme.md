Entity Framework
=========

Requirements
----
####Linux :
G++ with C++11 support. Tested on 4.8.1+

####Windows :
Microsoft Visual Studio 2013.2 + [ Might work on 2013 and 2013.1 ] 

####Mac OSX :
XCode with C+11 support. Tested on 5.1.2+


Samples
----

```C++
TODO : add sample code
```


License
----

**MIT License**

Authors
----
- Matteo Bracci

- Edoardo 'sparkon' Dominici

- Mattia Angelini